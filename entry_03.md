# IDP Engineering Logbook 

## ENTRY 03
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 4 (8th - 12th November 2021)


### Agenda

- Sprayer parameter calculations
- Discuss sprayer design and components

### Method

| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  | Discuss on sprayer components and design |
| Meeting 2  | Discussion with Mr Fiqri |



### Impact 
Findings from both meetings: 

**Meeting 1**

- Sprayer calculation and calibration sheet created by Thebban and Yamunan
 Nozzle:
- need to confirm Y-shape or L/T shape
- how many based on swath overlap (50-100% target)
- placement

2 sprayer system options: 
1. Ready-made system from aliexpress - ~RM2500
2. DIY with Poladrone stuff- ~RM1000

**Meeting 2**
- suggested pump with 8L/min might be too fast for HAU
- need to find out if normal water pump could be used instead of agri drone pumps 
- 5L tank is sufficient for small airship
- field speed is set to be slow as HAU moves slow and improves battery life

Important note: the system needs to be scalable, improvable, and modifiable




> _Discussion and Meeting 2 with Mr Fiqri_

<img src="/uploads/51de7b39b4c15345f3ccde02b278ac17/IMG_3648.jpg" alt="IMG_3648" width="300"/>

<img src="/uploads/964316e4d3a89a516cbc8267939803ee/IMG_5284.JPG" alt="IMG_5284" width="300"/>