# IDP Engineering Logbook 

## FAILURE/POST-MORTEM OVERVIEW
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 14  (23rd - 28th January 2022)

Generally, the project turned out reasonably successful as the goal of having the airship to fly was met. However, due to certain circumstances the airship tilted and punctured its skin causing it to deflate and crash.

### What went wrong?

| TECHNICALITIES | PLANNING, MANAGEMENT AND COMMUNICATION |
| ------ | ------ |
| <p align="center">issue with control systems configuration</p>  | <p align="center">project resources delay</p> |
| <p align="center">strong wind</p> | <p align="center">subsystem’s Gitlab Repository unaccessible by other subsystems</p>  |
| <p align="center">thruster arm-airship attachment method</p> |  |
| <p align="center">insufficient testing</p> |  |

### What went right? 
- project scope was well defined
- project schedule was realistic
- project team meetings are well-structured and well-organized
- subsytem functions well individually
