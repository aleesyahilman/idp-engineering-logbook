# IDP Engineering Logbook 

## ENTRY 13
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 14  (23rd - 28th January 2022)

### Agenda
- pump switch control (Software and Control subsystem)
- flight test 

### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  |<ul><li>Figure out the switch control of the pump with Software and Control subsystem representatives</li></ul> |
| Meeting 2  | <ul><li>Airship flight test</li><li>Post-mortem meeting</li></ul>  |



### Impact 

**Meeting 1**
- The pump is connected to a relay switch and wifi module.
- An app is created for the switch function. 
- The pump can be turned on and off remotely through the app with the help of the wifi module and Arduino coding.
- Further details on Software and Control subsystem's repository.
    - Video on the system's functionality: [click here](https://drive.google.com/file/d/1gQFDOEtGEtNFbpiytPoUBLqmeWlKCE2v/view?usp=sharing)


**Meeting 2**
- Pre-Flight checklist of sprayer system was done, and the system run smoothly.
- System attachment to airship was not conducted due to unfortunate circumstances. (refer failure report)

### Next Step
- Post-mortem and failure report