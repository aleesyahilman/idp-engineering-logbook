# IDP Engineering Logbook 

## ENTRY 04
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 5 (15th - 19th November 2021)


### Agenda

- Sprayer parameter calculations
- Finalize sprayer design and components

### Method

| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  | Discuss on sprayer components and design |
| Meeting 2  | Further discssion with Dr Salah and other subsystems |



### Impact 

**Meeting 1**
- decided on which tank, pump, nozzle and nozzle placement to use (Figure 1)
- updated our [tracker](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing) on the confirmed components (confirmed component is highlighted green) 


**Meeting 2**
- 8 nozzles might be too much, therefore nozzle placement is changed to Figure 2
    - reason is because more nozzle, more weight (redundant)
    - increases unnecessary load and pressure for the pump
- nozzles should be placed under the belly of airship to avoid pesticide dispersing due to downwash of propeller


### Next step
- nozzle placement sketch needs to be in 3 perspective view
- parameters we need: 
    - weight of whole sprayer system
    - suggestion of payload attachment to gondola
    - design or rough sketch of the whole system with its mounting
- The tracker will be sent to Mr Fiqri for further confirmation and product order





> _Figure 1: Suggested Nozzle Placement_

<img src="/uploads/ea6cd413bd26c55bc9da54bf267d95bd/2021-11-19.png" alt="2021-11-19" width="600"/>


> _Figure 2: Finalized Nozzle Placement_

<img src="/uploads/460c9af54869afccecd52454a025946b/2021-11-20.png" alt="2021-11-20" width="600"/>

