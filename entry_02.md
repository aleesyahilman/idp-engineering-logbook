# IDP Engineering Logbook 

## ENTRY 02
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 3 (1st - 5th November 2021)


### Agenda

- Conduct a survey to find the suitable sprayer system
- Drone company (AERODYNE) site visit to see how sprayer system works generally
- Conduct 2 meetings

### Method

| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  | AERODYNE visit |
| Meeting 2  | Briefing on findings from the site visit, list out items needed for the sprayer and create a ['Tracker'](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit#gid=0) for the sprayer system. |



### Impact 

Findings from AERODYNE visit:
- X5 drone is used for paddy pesticide spraying
    - detachable tank (10-12L capacity)
    - battery 22000 mAH (50%)
    - has 4 spray nozzles
    - pump has control systems
    - max altitude: 7-8m
    - 5% battery to climb
    - boom stick can cover one plot (1.2 hectares/12 lines)
    - full battery can spray up to 8 lines
    - pesticide used Takomi (powder) and 2M


> _AERODYNE visit and DJI Agras T16 drone_

<img src="/uploads/48976473c1e5331523019e2594503a3d/IMG_4844.jpg" alt="IMG_4844" width="300"/>

<img src="/uploads/0deaa45da3de133250611584894746c0/IMG_5069.JPG" alt="IMG_5069" width="300"/>


