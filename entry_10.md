# IDP Engineering Logbook 

## ENTRY 10
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 11 (3rd - 7th January 2022)


### Agenda

- pump and nozzle test
- flex tube and boom stick test  


### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  | Test run round 1:<ul><li>Assemble nozzles, pump and flex tube on pvc pipe</li><li>Run tests to see pump's functionality</li></ul> |
| Meeting 2  | Test run round 2:<ul><li>Assembled on carbon fibre boom stick</li><li>Observe the functionality of 'T' connector</li><li>Testing the functionality of 4 nozzzles</li></ul>  |
| Meeting 3  | Pump-tank and boomstick-tank attachment test  |

### Impact 

**Meeting 1**
- Nozzles and flextube are assembled on pvc pipe for first test run and only 2 nozzles are tested.
- The functionality of the pump and nozzles are tested on different conditions (ground level and elevated).
- As seen in Figure 1, the system is attached to the roof to mimic the airship height.
- The pump and nozzles are observed to function well.

> _Figure 1: Test Run 1_
<img src="/uploads/d3871e3fd1459f335e9178bcd7e08a14/WhatsApp_Image_2022-01-04_at_16.12.40.jpeg" alt="WhatsApp_Image_2022-01-04_at_16.12.40" width="400"/>



**Meeting 2**
- Nozzles and flextube are assembled on 1.29m long carbon fibre rods with 43cm nozzle spacing (Figure 2-1).
- Adding 'T' connector to connect 4 nozzles together (Figure 2-2).
- The functionality of the whole system is tested at ground level (Figure 2-3).
- It is observed that the nozzles leaked at a few parts.
- However, 'T' connector and pump function well.

> _Figure 2-1to3: Assembly and Test Run 2_
<img src="/uploads/71f82e68ded33f5a4cce0678be5b5a40/WhatsApp_Image_2022-01-05_at_15.56.59.jpeg" alt="WhatsApp_Image_2022-01-05_at_15.56.59" width="200"/>
<img src="/uploads/c1162a1359b1a7760dae4db432dfc736/WhatsApp_Image_2022-01-06_at_15.34.43.jpeg" alt="WhatsApp_Image_2022-01-06_at_15.34.43" width="400"/>
<img src="/uploads/ff362b4b0a33286953d017ca3a5771ab/IMG_6936.JPG" alt="IMG_6936" width="300"/>



**Meeting 3**
- A dummy tank is used to test different types of attachment styles (screws, clamps, nails etc).
- It is observed that expanding wall plugs and screws can withstand the pump well.
- It is also observed that the second tank which is thinner than orignal tank can hold the pump well without straining or cracking (Figure 3).


> _Figure 3: Attachment test_
<img src="/uploads/cc3b8d825a74e18e5bd52719d31be240/WhatsApp_Image_2022-01-07_at_17.06.50.jpeg" alt="WhatsApp_Image_2022-01-07_at_17.06.50" width="200"/>



### Next Step
- resolve leaking nozzle issue
- battery duration test
- find centre of gravity of whole system (FSI subsystem)
- pump switch control (Software and Control subsystem)
- attachment of whole system to tank 
