# IDP Engineering Logbook 

## ENTRY 09
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 10 (27th - 31st December 2021)

### Agenda
- Postponement of lectures/practicals/etc from 20/12/2021 to 2/1/2022 due to flood and covid-19 outbreak in H2.1 lab
- As the lab and faculty is closed, system assembly could not be done.
- However, the pump issue is resolved as we went to Poladrone to exchange it to new pump according to needed specifications. (Figure 1)

### Next Step
- pump and nozzle assembly and sprayer test run

> _Figure 1: Poladrone visit_
<img src="/uploads/bc194f0f7a5426a0435494569f2f7884/IMG_6541.JPG" alt="IMG_6541" width="400"/>
