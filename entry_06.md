# IDP Engineering Logbook 

## ENTRY 06
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 7 (29th November - 3rd December 2021)


### Agenda

- sprayer components confirmation 
- sprayer components order


### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  |<ul><li>Discussion with subsystem to reach consensus on the sprayer's parameters and which component is suitable such as pumps, nozzles etc.</li><li>create a new sheet in our tracker to finalize the components</li></ul>|
| Meeting 2  | Buy a few components for the mounting at the hardware store |


### Impact 

**Meeting 1**
- [tracker](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing) updated with the final list of components needed for the system
- tracker is sent to Mr Fiqri for component order placement

**Meeting 2**
- Components for the sprayer mounting such as the pvc pipe, elbow connectors, T connectors, and cable ties were bought 


### Next Step
- wait for components to arrive and start assembling the nozzle and pump system
- prepare tools needed for the mounting 


> _Figure 1: Receipt for components bought as stated in "Meeting 2"_
<img src="/uploads/97c4ab083dfedb76cecc0ae65698df18/WhatsApp_Image_2021-12-02_at_12.40.49.jpeg" alt="WhatsApp_Image_2021-12-02_at_12.40.49" width="400"/>