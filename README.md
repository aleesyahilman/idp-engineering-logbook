# IDP Engineering Logbook 

### Table of Content

| No. | Week | Entry | 
| ------ | ------ | ------ |
| 1 | 2 | [entry_01](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_01.md) |
| 2 | 3 | [entry_02](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_02.md) |
| 3 | 4 | [entry_03](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_03.md) |
| 4 | 5 | [entry_04](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_04.md) |
| 5 | 6 | [entry_05](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_05.md) |
| 6 | 7 | [entry_06](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_06.md) |
| 7 | 8 | [entry_07](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_07.md) |
| 8 | 9 | [entry_08](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_08.md) |
| 9 | 10 | [entry_09](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_09.md) |
| 10 | 11 | [entry_10](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_10.md) |
| 11 | 12 | [entry_11](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_11.md) |
| 12 | 13 | [entry_12](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_12.md)|
| 13 | 14 | <ul><li>[entry_13](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/entry_13.md) </li><li>[failure report](https://gitlab.com/aleesyahilman/idp-engineering-logbook/-/blob/main/failure_report.md)</li></ul>  |


