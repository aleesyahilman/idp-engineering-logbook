# IDP Engineering Logbook 

## ENTRY 05
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 6 (22nd - 26th November 2021)


### Agenda

- 2 perspective view sprayer system sketch
- payload attachment to gondola
- sprayer components confirmation 


### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  | Working in pairs/individually based on work division below  |
| Meeting 2  | Discussion with design subsystem on mounting attachment  |
| Meeting 3  | Discussion as a whole group to compile our work |


Work division
- Estimated Weight and Field Speed Query (Thebban)
- Calculations Transfer to Google Sheet (Yamunan)
- Mounting + Structure Design (Aleesya + Hanis) 
- Nozzle Jet Swath Query with Seller (Irfan) 
- Pesticide Mixing Ratio Calculation and conversion to GPA (Rafi)
- Total Calculation completion (Thebban + Yamunan)
- Presentation prep for next week (Rafi)



### Impact 

**Meeting 1**
- a 2-perspective-view sketch is produced (Figure 1)
- mounting components and material finalized as follows

refer numbering on Figure 2;

1) Sprayer mounting body and boom stick uses electrical pvc pipe as it is lighter than plumbing pvc pipe

2) 3 types of connectors ( elbow, T and 4 point). but 4 point might need to fabricate ourselves or order online as it is not available at the nearest hardware store

3) 3L tank as in picture. placed horizontally so we need to make new holes above (to pour in pesticide etc)

4) Velcro strap to secure tank in place

7) Pool float for landing gear as it is lightweight but compact. Inspired by a drone prototype in H2.1 lab

Not in figure: 
- flex tube to attach nozzles 
- cable ties to attach flex tube to boom stick
- pipe clamps to mount system to gondola

**Meeting 2**
- mounting attachment - use pipe clamp to bottom of gondola

**Meeting 3**
- [tracker](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing) updated
- parameters [calculation](https://docs.google.com/spreadsheets/d/1Ks4jATKlu6y6-r6O5OB6SzaOrM-6PxI-FWul_H_DW1I/edit?usp=sharing) of the sprayer system is updated
- empty weight estimation of the whole system is also calculated [(refer sheet 2)](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing)





> _Figure 1: 2-perspective-view sketch_

<img src="/uploads/61013b6ccd8b1aa8016b33e535f0ba22/WhatsApp_Image_2021-11-24_at_11.48.14.jpeg" alt="WhatsApp_Image_2021-11-24_at_11.48.14" width="400"/>
<img src="/uploads/2fe41da19ebbefc1859d580931f8ca58/WhatsApp_Image_2021-11-24_at_11.48.12.jpeg" alt="WhatsApp_Image_2021-11-24_at_11.48.12" width="400"/>


> _Figure 2: Mounting components and material_

<img src="/uploads/0363d2646141acd047fcadf5ca3b26f4/mounting_components.png" alt="mounting_components" width="700"/>

