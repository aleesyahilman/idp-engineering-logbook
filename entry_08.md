# IDP Engineering Logbook 

## ENTRY 08
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 9 (20th - 24th December 2021)

### Agenda
Postponement of lectures/practicals/etc from 20/12/2021 to 2/1/2022 due to flood and covid-19 outbreak in H2.1 lab

### Next Step
- resolve pump and nozzle issue 
- pump and nozzle assembly and sprayer test run
