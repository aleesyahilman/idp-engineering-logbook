# IDP Engineering Logbook 

## ENTRY 01
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 2 (25th - 29th October 2021)


### Agenda

- Project briefing and planning 
- HAU components checklist [('Tracker'.)](https://docs.google.com/spreadsheets/d/1ij4VMTHH0KphTnuX29gXc1W_kzoVliqFd0tuFXs60F4/edit?usp=sharing)
- Create Bill of Material (BOM) for budget planning
- Product availability check with suppliers


### Method

In order to create the checklist, we went to the lab and check each component's condition and availability. The checklist is created on Google Sheets so that it is easier to track. A list of unavailable or faulty components were also created (Bill of Material) to track the components or tools that we need to buy. A Gantt Chart was also created by each subsystem to keep track on their progress. 


### Impact 

The 'Tracker' and Gantt Chart created allows us to keep track of the status of each component of the HAU and the actions that should be taken by each subsystem. 


> _Tidying up and updating BOM_


<img src="/uploads/5b404dd25024baa09ee3460532d379fa/05a066b5-b913-4987-a3d3-27fe82a00e4a.JPG" alt="05a066b5-b913-4987-a3d3-27fe82a00e4a" width="200"/>

<img src="/uploads/b0c19b2814b8469fb9833a1bf48145d3/1dead508-0387-431e-af73-ccb9b632ed57.jpg" alt="1dead508-0387-431e-af73-ccb9b632ed57" width="200"/>

