# IDP Engineering Logbook 

## ENTRY 07
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 8 (13th - 17th December 2021)


### Agenda

- new airship-payload attachment design 
- pump and nozzle test
- flex tube and boom stick test  


### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  | Discuss and come up with new airship-payload attachment design according to the requirements listed below:<ul><li>Battery accessibility</li><li>Power board accessibility</li><li>Stability</li><li>Weight</li><li>Thruster arm connector</li><li>Attachment to airship location</li></ul>|


### Impact 

**Meeting 1**
- a new mounting design is developed and is updated into [tracker](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing) 
- 2 alternative designs are drafted 
- their estimated weight is calculated and updated into the [tracker](https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing) 
- their weight, and accessibility is compared
-  the best design is chosen (Figure 1) with the attachment as Figure 2


### Next Step
- resolve pump and nozzle issue 
- pump and nozzle assembly and sprayer test run


> _Figure 1: Final Design of sprayer system_
<img src="/uploads/7f88c16c30f7b1560fbbd7f9d1b814ec/WhatsApp_Image_2021-12-21_at_23.09.00.jpeg" alt="WhatsApp_Image_2021-12-21_at_23.09.00" width="400"/>

> _Figure 2: Suggested airship-payload attachment_
<img src="/uploads/2ba3bfa60a423ce9c8844c34abde01ef/WhatsApp_Image_2021-12-16_at_13.19.32.jpeg" alt="WhatsApp_Image_2021-12-16_at_13.19.32" width="400"/>