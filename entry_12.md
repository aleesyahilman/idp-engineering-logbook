# IDP Engineering Logbook 

## ENTRY 12
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 13  (17th - 20th January 2022)

### Agenda
- pump switch control (Software and Control subsystem)
- gondola attachment to tank

### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  |<ul><li>Brief on how to attach gondola to tank</li></ul> |
| Meeting 2  | <ul><li>Material procurement</li><li>Gondola attachment-system assembly on main tank</li></ul>  |



### Impact 

**Meeting 1**
- Suggested to add a plate and a truss system that would support the gondola (Figure 1)
- The gondola will be strapped onto the plate with velcro 

> _Figure 1: Rough sketch of gondola-tank attachment_
<img src="/uploads/2cbb1abb94195967d60ab67c4be052a6/WhatsApp_Image_2022-01-13_at_23.53.25.jpeg" alt="WhatsApp_Image_2022-01-13_at_23.53.25" width="400"/>



**Meeting 2**
- Plywood is used as the plate that would support the gondola
- 'L' bracket is added at the end of the tank to support the extended part of the gondola 
- Plywood is attached onto the tank using epoxy and double sided tape
- 'L' bracket is screwed into the tank (Figure 2-1)
- Velcro strap added onto the plate for gondola attachment (Figure 2-2)

> _Figure 2-1: 'L' Bracket screwed onto tank_
<img src="/uploads/d71503391f0920779e930ee3d1a4752a/WhatsApp_Image_2022-01-20_at_15.01.45.jpeg" alt="WhatsApp_Image_2022-01-20_at_15.01.45" width="200"/>

> _Figure 2-2: Velcro strap on attached plate_
<img src="/uploads/9e2e4a98190bcd389017ebcd918e56db/WhatsApp_Image_2022-01-21_at_12.19.51.jpeg" alt="WhatsApp_Image_2022-01-21_at_12.19.51" width="250"/>

### Next Step
- flight test