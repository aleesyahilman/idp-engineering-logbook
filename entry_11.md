# IDP Engineering Logbook 

## ENTRY 11
> Nurul Aleesya binti Hilman Syahputra (196887) 
>
> Subsystem: Cropsprayer Payload Design
>
> Group: 4
>
> Week 12  (10th - 14th January 2022)


### Agenda

- resolve leaking nozzle issue
- whole system assembly on main tank
- battery duration test



### Method


| MEETINGS | PURPOSE |
| ------ | ------ |
| Meeting 1  |<ul><li>Waterproofing tape test on dummy tank</li><li>Boomstick clamp attachment test on dummy tank</li></ul> |
| Meeting 2  | <ul><li>System assembly on main tank</li><li>System test run round 3 on main tank</li></ul>  |
| Meeting 3  | System test round 4 to see battery usage and spraying duration  |



### Impact 

**Meeting 1**
- The waterproofing tape successfully stopped all the leakage occured before.
- Boomstick clamp attachment is successful on dummy tank (Figure 1).
- The same attachment methods will be applied onto the main tank in order to attach the pump and boomstick clamps.
> _Figure 1: Clamp attachment on dummy tank_
<img src="/uploads/a4a2b49476542cb4af69e95562b4627d/WhatsApp_Image_2022-01-11_at_15.10.33.jpeg" alt="WhatsApp_Image_2022-01-11_at_15.10.33" width="200"/>


**Meeting 2**
- System (pump, boomstick, flextube and nozzles) successfully assembled onto the main tank without any leakage. 
- The assembled system is able to run smoothly with a condition that residual pressure on the nozzles is released first. 

> _Figure 2: Assembled system on main tank_
<img src="/uploads/035a51a52137bed9c54a026c0af2d9d8/WhatsApp_Image_2022-01-12_at_18.52.37.jpeg" alt="WhatsApp_Image_2022-01-12_at_18.52.37" width="300"/>


**Meeting 3**
- The tank is filled with 5L of water and the time taken to empty the tank is observed. 
- Battery percentage before and after the test is also noted to observe it's battery usage.
- Battery specs:
    - LiPo 6S Battery
    - Capaciity: 

- Result from the tests are as follows:

| NO. OF TEST RUN | OPERATION DURATION | BATTERY USAGE |
| ------ | ------ | ------ |
| <p align="center">1</p> | <p align="center">1 min 20 secs</p> | <p align="center">7%</p> |
| <p align="center">2</p> | <p align="center">1 min 26 secs</p> | <p align="center">6%</p> |


> _Figure 3: Assembled system on main tank (top view)_
<img src="/uploads/f73bb23bd09de67d9bd6de675eb8d915/top_view.jpeg" alt="top_view" width="400"/>


### Next Step
- pump switch control (Software and Control subsystem)
- gondola attachment to tank
- attachment of whole system to airship
